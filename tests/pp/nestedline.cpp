/*@

requires \true;
// ensures \true;
*/
void m() {}

/*@

requires \true;
//@ ensures \true;
*/
void n() {}
